//
//  DetailViewController.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import UIKit

extension NSDate {
    func brFormat() -> String {
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy"
        return dayTimePeriodFormatter.stringFromDate(self)
    }
}

extension UIImageView {
    func loadAvatar(url: String) {
        self.sd_setImageWithURL(NSURL(string: (url)), placeholderImage: UIImage(named: "user"))
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
}

class DetailViewController: UITableViewController {
    
    var repository: Repository?
    var pullrequests = [PullRequest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = repository?.name
        
        load()
        
        self.tableView.registerNib(UINib.init(nibName: "PullRequestHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "pullrequest_header")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        self.refreshControl?.addTarget(self, action: #selector(DetailViewController.load), forControlEvents: UIControlEvents.ValueChanged)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load() {
        if let repo = repository {
            self.refreshControl?.beginRefreshing()
            GitHubService.sharedInstance.fetchPRfromRepository(repo) { (result) in
                self.pullrequests = result
                self.reloadTable()
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    func reloadTable() {
        if(self.pullrequests.count > 0) {
            self.tableView.reloadData()
        } else {
            let label = UILabel.init()
            label.text = "Nenhum pull request encontrado"
            label.textColor = UIColor.blackColor()
            label.textAlignment = .Center
            self.tableView.backgroundView = label
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.pullrequests.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier("pullrequest_header") as! PullRequestHeaderCell
        
        let pullrequest = self.pullrequests[section]
        
        if let date = pullrequest.date {
            cell.prDate.text = date.brFormat()
        }
        
        if let avatar = pullrequest.author?.avatar {
            cell.prAvatar.loadAvatar(avatar)
        }
        
        cell.prUsername.text = pullrequest.author?.name
        
        return cell
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("pullrequest_content") as! PullRequestContentCell
        
        let pullrequest = self.pullrequests[indexPath.section]
        
        cell.prBody.text = pullrequest.body
        cell.prTitle.text = pullrequest.title
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let pullrequest = self.pullrequests[indexPath.section]
        
        if let stringURL = pullrequest.url {
            if let url = NSURL.init(string: stringURL) {
                if UIApplication.sharedApplication().canOpenURL(url) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
        }
    }
    
}
