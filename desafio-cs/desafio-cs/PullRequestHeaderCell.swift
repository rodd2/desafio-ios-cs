//
//  PullRequestHeaderCell.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/23/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import UIKit

class PullRequestHeaderCell: UITableViewHeaderFooterView {
    @IBOutlet weak var prUsername : UILabel!
    @IBOutlet weak var prDate : UILabel!
    @IBOutlet weak var prAvatar: UIImageView!
}
