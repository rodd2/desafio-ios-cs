//
//  PullRequestContentCell.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/23/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import UIKit

class PullRequestContentCell: UITableViewCell {
    @IBOutlet weak var prTitle: UILabel!
    @IBOutlet weak var prBody: UILabel!
}
