//
//  GithubService.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import Alamofire
import Gloss

class GitHubService: NSObject {
    
    static let sharedInstance = GitHubService()
    var page = 1;
    
    private override init() {
        super.init()
    }
    
    func loadRepositories(completion: (result: [Repository]) -> Void) {
        Alamofire.request(.GET, "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)", parameters: nil)
            .responseJSON { response in
                
                switch response.result {
                    
                case .Success(let json):
                    let repositories = RepositoryResponse(json : json as! JSON)
                    self.page = self.page + 1
                    
                    completion(result: (repositories?.repositories)!)
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    
                    completion(result: [Repository]())
                }
        }
    }
    
    func fetchPRfromRepository(repository: Repository, completion: (result: [PullRequest]) -> Void) {
        if let user = repository.owner?.name, name = repository.name {
            Alamofire.request(.GET, "https://api.github.com/repos/\(user)/\(name)/pulls", parameters: nil)
                .responseJSON { response in
                    
                    switch response.result {
                        
                    case .Success(let json):
                        let pr = PullRequestResponse(jsonArray: json as! [AnyObject])
                        completion(result: (pr?.pullrequests)!)
                        
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                        
                        completion(result: [PullRequest]())
                    }
            }
        } else {
            completion(result: [PullRequest]())
        }
    }
}
