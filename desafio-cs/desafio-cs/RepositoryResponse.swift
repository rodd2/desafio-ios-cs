//
//  RepositoryResponse.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import Gloss

struct RepositoryResponse : Glossy {
    
    var repositories: [Repository]?
    
    init?(json: JSON) {
        self.repositories = "items" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "items" ~~> self.repositories
            ])
    }
}
