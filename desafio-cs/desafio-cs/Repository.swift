//
//  Repository.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import Gloss

struct Repository : Glossy {
    
    let name: String?
    let description: String?
    let owner: User?
    let star: Int?
    let fork: Int?
    
    init?(json: JSON) {
        self.name = "name" <~~ json
        self.description = "description" <~~ json
        self.owner = "owner" <~~ json
        self.star = "stargazers_count" <~~ json
        self.fork = "forks" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "description" ~~> self.description,
            "owner" ~~> self.owner,
            "stargazers_count" ~~> self.star,
            "forks" ~~> self.fork
            ])
    }

}