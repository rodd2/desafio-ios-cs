//
//  User.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import Gloss

struct User : Glossy {
    
    let name: String?
    let avatar: String?
    
    init?(json: JSON) {
        self.name = "login" <~~ json
        self.avatar = "avatar_url" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "login" ~~> self.name,
            "avatar_url" ~~> self.avatar
            ])
    }
}