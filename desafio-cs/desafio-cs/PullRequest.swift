//
//  PullRequest.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import Gloss

extension Decoder {
    static func decodeGitHubDate(key: String, json: JSON) -> NSDate? {
        
        if let string = json.valueForKeyPath(key) as? String {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            return (dateFormatter.dateFromString(string))
        }
        
        return nil
    }
}

struct PullRequest : Glossy {
    
    let author: User?
    let title: String?
    let date: NSDate?
    let body : String?
    let url : String?
    
    init?(json: JSON) {
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        self.date = Decoder.decodeGitHubDate("created_at", json: json)
        self.author = "user" <~~ json
        self.title = "title" <~~ json
        self.body = "body" <~~ json
        self.url = "html_url" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "user" ~~> self.author,
            "title" ~~> self.title,
            "updated_at" ~~> self.date,
            "body" ~~> self.body,
            "html_url" ~~> self.url
            ])
    }
}
