//
//  RepositoryContentCell.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/23/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import UIKit

class RepositoryContentCell: UITableViewCell {
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var repoDescription: UILabel!
    @IBOutlet weak var repoStar: UILabel!
    @IBOutlet weak var repoFork: UILabel!
    
}
