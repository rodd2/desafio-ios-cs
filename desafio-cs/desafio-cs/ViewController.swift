//
//  ViewController.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UITableViewController{
    
    var repositories = [Repository]()
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        load()
        
        self.tableView.registerNib(UINib.init(nibName: "RepositoryHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "repository_header")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
 
        self.refreshControl?.addTarget(self, action: #selector(ViewController.load), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load() {
        self.refreshControl?.beginRefreshing()
        GitHubService.sharedInstance.loadRepositories { (result) in
            self.repositories.appendContentsOf(result)
            self.refreshControl?.endRefreshing()
            self.reloadTableView()
            self.bottomView.hidden = true
        }
    }
    
    func reloadTableView() {
        if(self.repositories.count > 0) {
            tableView.reloadData()
        } else {
            let label = UILabel.init()
            label.text = "Nenhum repositório encontrado"
            label.textColor = UIColor.blackColor()
            label.textAlignment = .Center
            self.tableView.backgroundView = label
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.repositories.count
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier("repository_header") as! RepositoryHeaderCell
        
        let repository = self.repositories[section]
        
        cell.repoUsername.text = repository.owner?.name
        
        if let avatar = repository.owner?.avatar {
            cell.repoAvatar.loadAvatar(avatar)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("repository_content") as! RepositoryContentCell
        
        let repository = self.repositories[indexPath.section]
        
        cell.repoName.text = repository.name
        
        if let star = repository.star {
            cell.repoStar.text = String(star)
        }
        
        if let fork = repository.fork {
            cell.repoFork.text = String(fork)
        }
        
        cell.repoDescription.text = repository.description
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let detailViewController = storyBoard.instantiateViewControllerWithIdentifier("detail_view_controller") as! DetailViewController
        
        detailViewController.repository = self.repositories[indexPath.section]
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section + 5) == self.repositories.count {
            self.bottomView.hidden = false
            load()
        }
    }
    
}