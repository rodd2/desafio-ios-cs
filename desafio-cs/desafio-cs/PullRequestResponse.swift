//
//  PullRequestResponse.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/22/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import Gloss

struct PullRequestResponse : Glossy {
    
    var pullrequests: [PullRequest]?
    
    init?(jsonArray: [AnyObject]) {
        let jsonDictionary = ["pull_requests" : jsonArray]
        
        self.init(json: jsonDictionary)
    }
    
    init?(json: JSON) {
        self.pullrequests = "pull_requests" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "pull_requests" ~~> self.pullrequests
            ])
    }
}
