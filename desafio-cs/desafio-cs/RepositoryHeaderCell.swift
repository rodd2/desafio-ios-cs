//
//  RepositoryHeaderCell.swift
//  desafio-cs
//
//  Created by Rodrigo Cavalcante on 8/23/16.
//  Copyright © 2016 Rodrigo Cavalcante. All rights reserved.
//

import UIKit

class RepositoryHeaderCell: UITableViewHeaderFooterView {
    @IBOutlet weak var repoUsername: UILabel!
    @IBOutlet weak var repoAvatar: UIImageView!    
}
